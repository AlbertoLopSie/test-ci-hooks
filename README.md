# test ci hooks

Repo para probar los webhooks de CI

Este repo tiene un pipeline de 2 etapas: build y deploy

La primera etapa tiene un job build_job que se inicia con cada commit a master. 
Este job solo se ejecuta si el pipeline no fue niciado con trigger (except: triggers)

El job deploy_job de la segfunda etapa (deploy) sólo se ejecuta cuando el pipeline
es iniciado cn un trigger (only: triggers)

El trigger se dispara con un llamado POST con la siguiente forma:

curl -X POST -F token=35af4dc0215623cc5eabd45ed20ff7 -F ref=master https://gitlab.com/api/v4/projects/16195403/trigger/pipeline
curl -X POST "https://gitlab.com/api/v4/projects/16195403/trigger/pipeline?token=35af4dc0215623cc5eabd45ed20ff7&ref=master"

Este POST podría invocarse con un Expo Webhook

expo webhooks:set --event build --secret expobuildtestcihook --url "https://gitlab.com/api/v4/projects/16195403/trigger/pipeline?token=35af4dc0215623cc5eabd45ed20ff7&ref=master"

(NO ESTA ANDANDO)

Otra opción es disparar un job manual como se explica en https://docs.gitlab.com/ee/api/jobs.html#play-a-job

curl -X POST --header "PRIVATE-TOKEN: 35af4dc0215623cc5eabd45ed20ff7" "https://gitlab.com/api/v4/projects/16195403/jobs/1/play"



